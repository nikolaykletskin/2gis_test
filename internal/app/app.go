package app

import (
	"context"
	"errors"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"applicationDesignTest/internal/middleware/logging"
	orderRepository "applicationDesignTest/internal/repository/order"
	roomAvailabilityRepository "applicationDesignTest/internal/repository/room_availability"
	orderService "applicationDesignTest/internal/service/order"
	"applicationDesignTest/internal/transport/http/order"
	"applicationDesignTest/pkg/logger"
)

type App struct {
	logger logger.Interface
}

func NewApp(logger logger.Interface) *App {
	return &App{logger}
}

func (a *App) Run(ctx context.Context) error {
	ctx, stop := signal.NotifyContext(ctx, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)
	defer stop()
	or := orderRepository.NewRepository()
	rar := roomAvailabilityRepository.NewRepository()
	ors := orderService.NewService(or, rar, a.logger)
	oh := order.NewHandler(ctx, a.logger, ors)

	mux := http.NewServeMux()
	mux.HandleFunc("POST /orders", oh.Create)
	lm := logging.NewLoggingMiddleware(mux, a.logger)

	srv := http.Server{}
	srv.Addr = ":8080"
	srv.Handler = lm

	errChan := make(chan error)
	go func() {
		a.logger.Info("Server is listening on localhost:8080")
		if err := srv.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			a.logger.Error("listen and serve returned err: %v", err)
			errChan <- err
		}
	}()

	for {
		select {
		case <-ctx.Done():
			if err := srv.Shutdown(context.TODO()); err != nil {
				a.logger.Info("server shutdown returned an err: %v\n", err)
				return err
			}
			return nil
		case err := <-errChan:
			return err
		}
	}
}
