package order

import (
	"context"
	"errors"
	"time"

	"applicationDesignTest/internal/entity"
	"applicationDesignTest/pkg/date"
)

func (s *service) Create(ctx context.Context, order *entity.Order) (*entity.Order, error) {
	daysToBook := date.DaysBetween(order.From, order.To)
	unavailableDays := make(map[time.Time]struct{})

	for _, day := range daysToBook {
		unavailableDays[day] = struct{}{}
	}

	roomAvailability, _ := s.roomAvailabilityRepository.GetAll(ctx)

	for _, dayToBook := range daysToBook {
		for i, availability := range roomAvailability {
			if !availability.Date.Equal(dayToBook) || availability.Quota < 1 {
				continue
			}
			roomAvailability[i].Quota -= 1
			delete(unavailableDays, dayToBook)
		}
	}

	if len(unavailableDays) != 0 {
		s.logger.Error("Hotel room is not available for selected dates", "order", order, "dates", unavailableDays)
		return nil, errors.New("hotel room is not available for selected dates")
	}

	err := s.orderRepository.Create(ctx, order)
	s.roomAvailabilityRepository.UpdateAll(ctx, roomAvailability)
	if err != nil {
		s.logger.Error(err.Error())
		return nil, err
	}
	s.logger.Info("Order successfully created", "order", *order)

	return order, nil
}
