package order

import (
	"context"

	"applicationDesignTest/internal/entity"
	"applicationDesignTest/pkg/logger"
)

type orderRepository interface {
	Create(ctx context.Context, order *entity.Order) error
}

type roomAvailabilityRepository interface {
	GetAll(ctx context.Context) ([]entity.RoomAvailability, error)
	UpdateAll(ctx context.Context, data []entity.RoomAvailability) error
}

type service struct {
	orderRepository            orderRepository
	roomAvailabilityRepository roomAvailabilityRepository
	logger                     logger.Interface
}

func NewService(or orderRepository, rar roomAvailabilityRepository, logger logger.Interface) *service {
	return &service{
		orderRepository:            or,
		roomAvailabilityRepository: rar,
		logger:                     logger,
	}
}
