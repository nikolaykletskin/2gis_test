package roomavailability

import (
	"context"
	"sync"

	"applicationDesignTest/internal/entity"
	"applicationDesignTest/pkg/date"
)

type repository struct {
	data []entity.RoomAvailability
	mtx  sync.Mutex
}

func NewRepository() *repository {
	data := []entity.RoomAvailability{
		{HotelID: "reddison", RoomID: "lux", Date: date.New(2024, 1, 1), Quota: 1},
		{HotelID: "reddison", RoomID: "lux", Date: date.New(2024, 1, 2), Quota: 1},
		{HotelID: "reddison", RoomID: "lux", Date: date.New(2024, 1, 3), Quota: 1},
		{HotelID: "reddison", RoomID: "lux", Date: date.New(2024, 1, 4), Quota: 1},
		{HotelID: "reddison", RoomID: "lux", Date: date.New(2024, 1, 5), Quota: 0},
	}

	return &repository{
		data: data,
	}
}

func (rar *repository) GetAll(_ context.Context) ([]entity.RoomAvailability, error) {
	rar.mtx.Lock()
	defer rar.mtx.Unlock()
	return rar.data, nil
}

func (rar *repository) UpdateAll(_ context.Context, data []entity.RoomAvailability) error {
	rar.mtx.Lock()
	defer rar.mtx.Unlock()
	rar.data = data
	return nil
}
