package order

import (
	"context"
	"sync"

	entity "applicationDesignTest/internal/entity"
)

type repository struct {
	data []entity.Order
	mtx  sync.RWMutex
}

func NewRepository() *repository {
	return &repository{}
}

func (or *repository) Create(_ context.Context, order *entity.Order) error {
	or.mtx.Lock()
	defer or.mtx.Unlock()
	or.data = append(or.data, *order)
	return nil
}
