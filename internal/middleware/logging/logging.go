package logging

import (
	"net/http"

	"applicationDesignTest/pkg/logger"
)

type LoggingMiddleware struct {
	logger  logger.Interface
	handler http.Handler
}

func NewLoggingMiddleware(handler http.Handler, logger logger.Interface) *LoggingMiddleware {
	return &LoggingMiddleware{
		logger:  logger,
		handler: handler,
	}
}

func (lm *LoggingMiddleware) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	lm.logger.LogRequest(r)
	lm.handler.ServeHTTP(w, r)
}
