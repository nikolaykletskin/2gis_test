package order

import (
	"encoding/json"
	"net/http"

	"applicationDesignTest/internal/entity"
)

func (h *Handler) Create(w http.ResponseWriter, r *http.Request) {
	var newOrder entity.Order

	err := json.NewDecoder(r.Body).Decode(&newOrder)

	// Validate

	if err != nil {
		h.logger.Error(err.Error())
		http.Error(w, "Invalid request", http.StatusBadRequest)
		return
	}

	createdOrder, err := h.orderService.Create(h.ctx, &newOrder)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	createdOrderJson, err := json.Marshal(createdOrder)
	if err != nil {
		h.logger.Error(err.Error())
		http.Error(w, "Server error", http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	w.Write(createdOrderJson)
}
