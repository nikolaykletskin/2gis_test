package order

import (
	"context"

	"applicationDesignTest/internal/entity"
	"applicationDesignTest/pkg/logger"
)

type orderService interface {
	Create(ctx context.Context, order *entity.Order) (*entity.Order, error)
}

type Handler struct {
	ctx          context.Context
	logger       logger.Interface
	orderService orderService
}

func NewHandler(ctx context.Context, logger logger.Interface, os orderService) *Handler {
	return &Handler{ctx, logger, os}
}
