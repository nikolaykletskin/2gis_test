package logger

import (
	"log/slog"
	"net/http"
	"os"
)

type Interface interface {
	Info(format string, v ...any)
	Error(format string, v ...any)
	LogRequest(r *http.Request)
}

type Logger struct {
	logger *slog.Logger
}

func NewLogger() *Logger {
	return &Logger{
		logger: slog.New(slog.NewJSONHandler(os.Stdout, nil)),
	}
}

func (l *Logger) LogRequest(r *http.Request) {
	l.logger.Info("Incoming HTTP request", slog.Group("request", "method", r.Method, "url", r.URL.String()))
}

func (l *Logger) Info(msg string, args ...any) {
	l.logger.Info(msg, args...)
}

func (l *Logger) Error(msg string, args ...any) {
	l.logger.Error(msg, args...)
}
