package main

import (
	"context"
	"log"

	"applicationDesignTest/internal/app"
	"applicationDesignTest/pkg/logger"
)

func main() {
	ctx := context.Background()
	logger := logger.NewLogger()
	a := app.NewApp(logger)
	err := a.Run(ctx)
	if err != nil {
		log.Fatalf(err.Error())
	}
}
